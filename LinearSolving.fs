﻿module LinearSolving

open FluidUtilities
open EdgeHandling
open Tensor
///An iterative solver for linear systems, implemented with for loops.
let linSolve nsolversteps (s: float32[][]) b a c =
    let g = (Array.length s)

    let rec linSolveInner nsolversteps (s1: float32[][]) (tmp: float32[][]) =
        match nsolversteps with
        | 0 -> s1
        | _ ->
            for i in 0..(Array.length s1 - 1) do
                for j in 0..(Array.length s1 - 1) do
                    tmp.[i].[j] <- 

                        //Edge handling is implemented here instead of called in FluidUtilities.
                        //Even though it is implemented as inline in FluidUtilities it still affected performance in my testing.
                        if inside i j g then
                            (s.[i].[j] + a * (s1.[(i-1)].[j] + s1.[(i+1)].[j] + s1.[i].[(j - 1)] + s1.[i].[(j+1)])) / c
                        elif inOutSideCorner i j g then
                            0.0f
                        else
                            let (i1, j1) = outerMostInnerCell i j g 
                            let result = (s.[i1].[j1] + a * (s1.[(i1-1)].[j1] + s1.[(i1+1)].[j1] + s1.[i1].[(j1 - 1)] + s1.[i1].[(j1+1)])) / c
                            match getAxisOfEdgeCell i j g with
                            | Horizontal -> if b = 1 then -result else result
                            | Vertical -> if b = 2 then -result else result

            linSolveInner (nsolversteps - 1) tmp s1

    linSolveInner nsolversteps (emptyGrid g) (emptyGrid g)

///A cache-aware, iterative solver for linear systems.
let linSolveAware z nsolversteps (s: float32[][]) b a c =
    let g = (Array.length s) //This is not great since it will end up with more bounds checking
    let g' = g - 1           //but the code would look horrible without it.

    let rec linSolveInner nsolversteps (s1: float32[][]) (tmp: float32[][]) =
        match nsolversteps with
        | 0 -> s1
        | _ ->  
            for i' in 0..(g'/z) do
                for j' in 0..(g'/z) do
                    let i',j' = i'*z, j'*z
                    for i=i' to min (i' + z - 1) g' do
                        for j=j' to min (j' + z - 1) g' do
                            //Edge handling is implemented here instead of called in FluidUtilities.
                            //Even though it is implemented as inline in FluidUtilities it still affected performance in my testing.
                            tmp.[i].[j] <-                                  
                                if inside i j g then                        
                                    (s.[i].[j] + a * (s1.[(i-1)].[j] + s1.[(i+1)].[j] + s1.[i].[(j - 1)] + s1.[i].[(j+1)])) / c
                                elif inOutSideCorner i j g then
                                    0.0f
                                else
                                    let (ix, jx) = outerMostInnerCell i j g 
                                    let result = (s.[ix].[jx] + a * (s1.[(ix-1)].[jx] + s1.[(ix+1)].[jx] + s1.[ix].[(jx - 1)] + s1.[ix].[(jx+1)])) / c
                                    match getAxisOfEdgeCell i j g with
                                    | Horizontal -> if b = 1 then -result else result
                                    | Vertical -> if b = 2 then -result else result

            linSolveInner (nsolversteps - 1) tmp s1

    linSolveInner nsolversteps (emptyGrid g)  (emptyGrid g) 



///A cache-oblivious, iterative solver for linear systems.
let linSolveOblivious threshold nsolversteps (s: float32[][]) b a c =
    let g = (Array.length s)//This is not great since it will end up with more bounds checking
                            //but the code would look horrible without it.

    let rec linSolveInner nsolversteps (s1: float32[][]) (tmp: float32[][]) =
        match nsolversteps with
        | 0 -> s1
        | _ ->  
            let rec loop(i0, i1, j0, j1) =
                let di, dj = i1 - i0, j1 - j0
                match (di >= dj), (di >= threshold), (dj >= threshold)  with
                | true,true,_ -> 
                        let mi = i0 + (di / 2)
                        loop (i0, mi, j0, j1)
                        loop (mi, i1, j0, j1)
                | _,_,true ->
                        let mj = j0 + (dj / 2)
                        loop (i0, i1, j0, mj)
                        loop (i0, i1, mj, j1)
                | _,_,_ ->
                        for i=i0 to i1-1 do
                            for j=j0 to j1-1 do
                                //Edge handling is implemented here instead of called in FluidUtilities.
                                //Even though it is implemented as inline in FluidUtilities it still affected performance in my testing.
                                tmp.[i].[j] <-
                                    if inside i j g then
                                        (s.[i].[j] + a * (s1.[(i-1)].[j] + s1.[(i+1)].[j] + s1.[i].[(j - 1)] + s1.[i].[(j+1)])) / c
                                    elif inOutSideCorner i j g then
                                        0.0f
                                    else
                                        let (ix, jx) = outerMostInnerCell i j g 
                                        let result = (s.[ix].[jx] + a * (s1.[(ix-1)].[jx] + s1.[(ix+1)].[jx] + s1.[ix].[(jx - 1)] + s1.[ix].[(jx+1)])) / c
                                        match getAxisOfEdgeCell i j g with
                                        | Horizontal -> if b = 1 then -result else result
                                        | Vertical -> if b = 2 then -result else result

            loop(0, g, 0, g)
            linSolveInner (nsolversteps - 1) tmp s1

    linSolveInner nsolversteps (emptyGrid g) (emptyGrid g) 



///An iterative solver for linear systems implemented using tensors handled by the CPU.
let linSolveTensorCPU nsolversteps s b a c =
    let g = s |> Array2D.length1 |> int64
    let a = HostTensor.filled [1L] a
    let c = HostTensor.filled [1L] c
    let s0 = HostTensor.ofArray2D s
    let s0middle  = s0.[1L..(g-2L), 1L..(g-2L)]

    let rec innerSolve nsolversteps0 (s1: Tensor<float32>) (tmp: Tensor<float32>) =
        if (nsolversteps0 = 0) then s1 
        else
            let tmpmiddle = tmp.[1L..(g-2L), 1L..(g-2L)]
            let up      = s1.[1L..(g-2L), 2L..(g-1L)]
            let down    = s1.[1L..(g-2L), 0L..(g-3L)]
            let left    = s1.[0L..(g-3L), 1L..(g-2L)]
            let right   = s1.[2L..(g-1L), 1L..(g-2L)]

            s1.FillMultiply a s1
            tmpmiddle.FillAdd s0middle up
            tmpmiddle.FillAdd tmpmiddle down
            tmpmiddle.FillAdd tmpmiddle left
            tmpmiddle.FillAdd tmpmiddle right
            tmpmiddle.FillDivide tmpmiddle c

            tmp.[0L, *]<- if b = 1 then -tmp.[1L, *] else tmp.[1L, *]
            tmp.[(g-1L), *]<- if b = 1 then -tmp.[(g-2L), *] else tmp.[(g-2L), *]
            tmp.[*, 0L]<- if b = 2 then -tmp.[*, 1L] else tmp.[*, 1L] 
            tmp.[*, (g-1L)]<- if b = 2 then -tmp.[*, (g-2L)] else tmp.[*, (g-2L)]
            innerSolve (nsolversteps0 - 1) tmp s1
    innerSolve nsolversteps (HostTensor.zeros<float32> [g; g]) (HostTensor.zeros<float32> [g; g]) |> HostTensor.toArray2D



///An iterative solver for linear systems implemented using tensors handled by the CPU.
///Does not work as intended on my system
let linSolveTensorGPU nsolversteps s b a c =
    let g = s |> Array2D.length1 |> int64
    let a = CudaTensor.filled [1L] a
    let c = CudaTensor.filled [1L] c
    let s0 = HostTensor.ofArray2D s |> CudaTensor.transfer
    let s0middle  = s0.[1L..(g-2L), 1L..(g-2L)]

    let rec innerSolve nsolversteps0 (s1: Tensor<float32>) (tmp: Tensor<float32>) =
        if (nsolversteps0 = 0) then s1 
        else
            let tmpmiddle = tmp.[1L..(g-2L), 1L..(g-2L)]
            let up      = s1.[1L..(g-2L), 2L..(g-1L)]
            let down    = s1.[1L..(g-2L), 0L..(g-3L)]
            let left    = s1.[0L..(g-3L), 1L..(g-2L)]
            let right   = s1.[2L..(g-1L), 1L..(g-2L)]

            s1.FillMultiply a s1
            tmpmiddle.FillAdd s0middle up
            tmpmiddle.FillAdd tmpmiddle down
            tmpmiddle.FillAdd tmpmiddle left
            tmpmiddle.FillAdd tmpmiddle right
            tmpmiddle.FillDivide tmpmiddle c

            tmp.[0L, *]<- if b = 1 then -tmp.[1L, *] else tmp.[1L, *]
            tmp.[(g-1L), *]<- if b = 1 then -tmp.[(g-2L), *] else tmp.[(g-2L), *]
            tmp.[*, 0L]<- if b = 2 then -tmp.[*, 1L] else tmp.[*, 1L] 
            tmp.[*, (g-1L)]<- if b = 2 then -tmp.[*, (g-2L)] else tmp.[*, (g-2L)]
            innerSolve (nsolversteps0 - 1) tmp s1
    innerSolve nsolversteps (CudaTensor.zeros<float32> [g; g]) (CudaTensor.zeros<float32> [g; g]) |> HostTensor.transfer 
    |> HostTensor.toArray2D