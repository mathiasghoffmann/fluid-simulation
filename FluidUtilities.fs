﻿module FluidUtilities

open Newtonsoft.Json
open System.IO

///Takes a staggered array and maps it into a nested list.
let arrayStaggeredToNestedList (stag: 'T[][]) = 
    [for column in stag do
        yield column |> List.ofArray]

///Takes an Array2D and maps it into a nested list.
let array2DToNestedList arr = 
    [let g = Array2D.length1 arr
    for row in 0..g-1  do
        yield arr.[row,*] |> List.ofArray]

///Prints small nested lists for use in debugging.
let printNestedList (list:float32 list list) = 
    printfn ""
    list |> List.map (fun nestedList -> 
        nestedList |> List.map (fun elem -> printf "%10.4f " elem)|> ignore; printfn "";) |> ignore
    printfn ""


let clamp255 x =
    if x < 0.0F then 0uy
    else if x > 255.0F then 255uy
    else byte x

let clamp x =
    if x < 0.0F then 0uy
    else if x > 1.0F then 255uy
    else byte x

let emptyGrid g = 
    Array.init (g) (fun _ ->
        Array.create (g) 0.0f)

///A type to hold information for use in fluid simulation.
///Originally made to be used in benchmarking.
type FluidData =
    {
     Name: string;
     U: float32[][];
     V: float32[][];
     D: float32[][];
     Nsteps: int;
     Nsolversteps: int;
     Timestep: float32;
     Diffusionrate: float32;
     Viscosity: float32;
     }
    override m.ToString() = m.Name

///Takes a path to a file containing fluid information and returns that information as FluidData
let fluidDataFromFile(path: string) =
    let data = File.ReadAllLines(path)
    {
    Name=(Array.last(path.Split "\\"));
    U=JsonConvert.DeserializeObject<float32[][]>(data.[0]);
    V=JsonConvert.DeserializeObject<float32[][]>(data.[1]);
    D=JsonConvert.DeserializeObject<float32[][]>(data.[2]);
    Nsteps=JsonConvert.DeserializeObject<int>(data.[3]);
    Nsolversteps=JsonConvert.DeserializeObject<int>(data.[4]);
    Timestep=JsonConvert.DeserializeObject<float32>(data.[5]);
    Diffusionrate=JsonConvert.DeserializeObject<float32>(data.[6]);
    Viscosity=JsonConvert.DeserializeObject<float32>(data.[7])
    }

///A type to hold information for use in fluid simulation.
///Made for us in benchmarking.
type Staggered =
    {
     Name: string;
     D: float32[][];
     Nsolversteps: int32;
     Timestep: float32;
     Diffusionrate: float32;
     }
    override m.ToString() = m.Name

///Takes a path to a file containing fluid information and returns that information as Staggered
let ReadFileToStaggered(file: string): Staggered =
    let data = File.ReadAllLines(file)
    {
    Name=(Array.last(file.Split "\\"));
    D=JsonConvert.DeserializeObject<float32[][]>(data.[0]);
    Nsolversteps=JsonConvert.DeserializeObject<int32>(data.[1]);
    Timestep=JsonConvert.DeserializeObject<float32>(data.[2]);
    Diffusionrate=JsonConvert.DeserializeObject<float32>(data.[3]);
    }

///A type to hold information for use in fluid simulation.
///Made for us in benchmarking.
type Arr2D =
    {
     Name: string;
     D: float32[,];
     Nsolversteps: int32;
     Timestep: float32;
     Diffusionrate: float32;
     }
    override m.ToString() = m.Name

///Takes a path to a file containing fluid information and returns that information as Arr2D
let ReadFileToArray2D(file: string): Arr2D =
    let data = File.ReadAllLines(file)
    {
    Name=(Array.last(file.Split "\\"));
    D=JsonConvert.DeserializeObject<float32[,]>(data.[0]);
    Nsolversteps=JsonConvert.DeserializeObject<int32>(data.[1]);
    Timestep=JsonConvert.DeserializeObject<float32>(data.[2]);
    Diffusionrate=JsonConvert.DeserializeObject<float32>(data.[3]);
    }