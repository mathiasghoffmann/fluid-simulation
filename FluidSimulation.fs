﻿module FluidSimulation

open FluidUtilities
open EdgeHandling
open LinearSolving

///A function for diffusing values in a grid s.
let diffuse linSolveFunction (b: int) (nsolversteps: int) (diffusionrateorviscosity: float32) (timestep: float32) s =
    let g = float32 ((Array.length s) - 2)
    let a = (timestep * diffusionrateorviscosity * g * g)
    linSolveFunction nsolversteps s b a (1.0f + 4.0f * a)

///A function for diffusing values in a grid s.
let diffuseArr2D linSolveFunction (b: int) (nsolversteps: int) (diffusionrateorviscosity: float32) (timestep: float32) s =
    let g = float32 ((Array2D.length1 s) - 2)
    let a = (timestep * diffusionrateorviscosity * g * g)
    linSolveFunction nsolversteps s b a (1.0f + 4.0f * a)

///A function for diffusing values in a grid s0.
let advect (b: int) timestep (u:float32[][]) (v:float32[][]) (s0:float32[][]) =
    let g = Array.length s0
    let timestep0 = timestep * float32 (int64 g - 2L)
    let s = emptyGrid g

    let edgeHandlingAdvect i j =
        let x = float32 i - timestep0 * u.[i].[j]
        let y = float32 j - timestep0 * v.[i].[j]
        let x = if x < 0.5F 
                then 0.5F 
                elif x > float32 (g - 2) + 0.5F 
                then float32 (g - 2) + 0.5F
                else x

        let y = if y < 0.5F 
                then 0.5F 
                elif y > float32 (g - 2) + 0.5F 
                then float32 (g - 2) + 0.5F
                else y

        let i0 = int x
        let i1 = i0 + 1

        let j0 = int y
        let j1 = j0 + 1
        let s1 = x - float32 i0
        let s0' = 1.0F - s1
        let t1 = y - float32 j0
        let t0 = 1.0F - t1
        (s0' * (t0 * s0.[i0].[j0] + t1 * s0.[i0].[j1])
         + s1 * (t0 * s0.[i1].[j0] + t1 * s0.[i1].[j1]))

    for i = 0 to (Array.length s - 1) do
        for j = 0 to (Array.length s  - 1) do
            let info = (s0, u, v, timestep0)
            s.[i].[j] <- edgeHandling i j g b edgeHandlingAdvect
    s




//A function for projecting values in a grids (u0, v0).
let project nsolversteps ((u0, v0): float32[][] * float32[][]) =
    let projecttop =
        let g = Array.length u0
        let s = emptyGrid g
        let projectTopCell i j = -0.5f * (u0.[i+1].[j] - u0.[i-1].[j] + v0.[i].[j+1] - v0.[i].[j-1]) / (float32 g)
        for i = 0 to (Array.length s - 1) do
            for j = 0 to (Array.length s - 1) do
                s.[i].[j] <- edgeHandling i j g 0 projectTopCell
        s

    let projectBottom (p0: float32[][]) (s0: float32[][]) b i0d j0d i1d j1d =
        let g = Array.length s0
        let s = emptyGrid g
        let projectBottomCell i j = (s0.[i].[j] - 0.5F * float32 (g-2) * (p0.[i+i0d].[j+j0d] - p0.[i+i1d].[j+j1d]))
        for i = 0 to (Array.length s - 1) do
            for j = 0 to (Array.length s - 1) do
                s.[i].[j] <- edgeHandling i j g b projectBottomCell
        s

    let projectBottomHorizontalAndVertical p0 =
        projectBottom p0 u0 1 1 0 (-1) 0,
        projectBottom p0 v0 2 0 1 0 (-1)

    linSolve nsolversteps projecttop 0 1.0F 4.0F
    |> projectBottomHorizontalAndVertical

//Calculates new values for densities d0 and velocities u0 v0.
let step linSolver u0 v0 d0 nsolversteps timestep diffusionrate viscosity = 
    let diffuseHorizontalAndVertical (u, v) =
        diffuse linSolver 1 nsolversteps viscosity timestep u,
        diffuse linSolver 2 nsolversteps viscosity timestep v

    let advectHorizontalAndVertical (u, v) = 
        advect 1 timestep u v u,
        advect 2 timestep u v v

    let calculateVelocities = 
        diffuseHorizontalAndVertical
        >> project nsolversteps
        >> advectHorizontalAndVertical
        >> project nsolversteps

    let calculateDenseties = 
        diffuse linSolver 0 nsolversteps diffusionrate timestep 
        >> advect 0 timestep u0 v0
    
    let u1, v1 = calculateVelocities (u0, v0)
    let d1 = calculateDenseties d0
    (u1, v1, d1)